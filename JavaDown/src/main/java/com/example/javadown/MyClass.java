package com.example.javadown;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import okhttp3.*;

public class MyClass {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void main(String[] args) throws IOException {
        File file = new File("E:\\ClassAB\\", "a.txt");
        file.createNewFile();
//        new MyClass().getRequest();
    }

    public void getRequest() throws IOException {
        OkHttpClient client = new OkHttpClient();

        String bvid = "",cid = "";
        bvid = "1Gr4y1A7Ln";
        cid = "299899316";

        /**
         * get cid : https://api.bilibili.com/x/web-interface/view?bvid=
         * get origin video link : https://api.bilibili.com/x/player/playurl?otype=json&fnver=0&fnval=2&player=1&qn=64&bvid=Bvid&cid=Cid
         */
        Request request = new Request.Builder()
                .url("https://api.bilibili.com/x/web-interface/view?bvid="+bvid)
                .build();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
//            System.out.println(response.body().string());
            String responseBody = response.body().string();
            JSONObject jsonObject = new JSONObject(responseBody);
            //get cid
            cid = jsonObject.getJSONObject("data").get("cid").toString();
        }
        //qn ： 视频质量         112 -> 高清 1080P+,   80 -> 高清 1080P,   64 -> 高清 720P,  32  -> 清晰 480P,  16 -> 流畅 360P
        // 最高支持 1080p,  1080P+是不支持的
        int qn = 16;
        request = new Request.Builder()
                .url("https://api.bilibili.com/x/player/playurl?otype=json&fnver=0&fnval=2&player=1" +
                        "&bvid=" + bvid +
                        "&cid=" + cid +
                        "&qn=" + qn)
                .build();

        response = client.newCall(request).execute();

        String newURL = "";

        if (response.isSuccessful()) {
//            System.out.println(response.body().string());
            String responseBody = response.body().string();
            JSONObject jsonObject = new JSONObject(responseBody);
            //get cid
            newURL = jsonObject.getJSONObject("data").getJSONArray("durl").getJSONObject(0).get("url").toString();
//            System.out.println(newURL);
        }

        request = new Request.Builder()
                .addHeader("Referer", "https://www.bilibili.com")
//                .addHeader("Sec-Fetch-Mode", "no-cors")
//                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36")
                .url(newURL)
                .build();

        response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            System.out.println(response.code());
//            System.out.println(response.body().byteStream());
            InputStream inputStream = response.body().byteStream();

            FileUtils.copyInputStreamToFile(inputStream, new File("E:\\ClassAB\\" + "third"));

//            String responseBody = response.body().string();
//            JSONObject jsonObject = new JSONObject(responseBody);
//            //get cid
//            cid = jsonObject.getJSONObject("data").get("cid").toString();
        } else {
            System.out.println("failure");
        }
//        downloadNew(newURL,"second");

    }


//    这个类使用了commons-io   网上下的参考
//    public void downloadNew(String urlPath, String descFileName) throws MalformedURLException {
//        InputStream inputStream = null;
//        try {
//            long begin = System.currentTimeMillis();
//            URL url = new URL(urlPath);
//            URLConnection urlConnection = url.openConnection();
//            urlConnection.setRequestProperty("Referer", "https://www.bilibili.com"); // 设置协议
//            urlConnection.setRequestProperty("Sec-Fetch-Mode", "no-cors");
//            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36");
//            urlConnection.setConnectTimeout(10 * 1000);
//            System.out.println("共:" + (urlConnection.getContentLength() / 1024) + "Kb");
//            System.out.println("开始下载...");
//            InputStream input = urlConnection.getInputStream();
//            FileUtils.copyInputStreamToFile(input, new File("E:\\ClassAB\\" + descFileName));
//            long end = System.currentTimeMillis();
//            System.out.println("耗时：" + (end - begin) / 1000 + "秒");
//            System.out.println("下载完成！");
//        } catch (Exception e) {
//            System.out.println("异常中止: " + e);
//        }
//    }

    /**
     * https://api.bilibili.com/x/web-interface/view?bvid=
     * {
     * 	"code": 0,
     * 	"message": "0",
     * 	"ttl": 1,
     * 	"data": {
     * 		"bvid": "BV1Gr4y1A7Ln",
     * 		"aid": 756822361,
     * 		"videos": 1,
     * 		"tid": 31,
     * 		"tname": "翻唱",
     * 		"copyright": 2,
     * 		"pic": "http://i1.hdslb.com/bfs/archive/a74a2af22ed5045ab44ba5bdb0baf2bb4ee450ad.jpg",
     * 		"title": "【阿梓】《最长的电影》都快听哭了，神回歌曲",
     * 		"pubdate": 1613747308,
     * 		"ctime": 1613747308,
     * 		"desc": "bilibili频道：阿梓从小就很可爱\nhttps://space.bilibili.com/7706705/\n微博：阿梓从小就很可爱\n网易云：阿梓从小就很可爱\n切片来自2020.12.22\n感觉挺好听的。",
     * 		"desc_v2": [{
     * 			"raw_text": "bilibili频道：阿梓从小就很可爱\nhttps://space.bilibili.com/7706705/\n微博：阿梓从小就很可爱\n网易云：阿梓从小就很可爱\n切片来自2020.12.22\n感觉挺好听的。",
     * 			"type": 1,
     * 			"biz_id": 0
     *                }],
     * 		"state": 0,
     * 		"duration": 271,
     * 		"rights": {
     * 			"bp": 0,
     * 			"elec": 0,
     * 			"download": 1,
     * 			"movie": 0,
     * 			"pay": 0,
     * 			"hd5": 0,
     * 			"no_reprint": 0,
     * 			"autoplay": 1,
     * 			"ugc_pay": 0,
     * 			"is_cooperation": 0,
     * 			"ugc_pay_preview": 0,
     * 			"no_background": 0,
     * 			"clean_mode": 0,
     * 			"is_stein_gate": 0,
     * 			"is_360": 0
     *        },
     * 		"owner": {
     * 			"mid": 1091772183,
     * 			"name": "1927的索尔维",
     * 			"face": "http://i0.hdslb.com/bfs/face/9836d9ed85b2e79a7ef5db32d6817ef23fa6310a.jpg"
     *        },
     * 		"stat": {
     * 			"aid": 756822361,
     * 			"view": 83249,
     * 			"danmaku": 53,
     * 			"reply": 124,
     * 			"favorite": 2257,
     * 			"coin": 504,
     * 			"share": 482,
     * 			"now_rank": 0,
     * 			"his_rank": 0,
     * 			"like": 1956,
     * 			"dislike": 0,
     * 			"evaluation": "",
     * 			"argue_msg": ""
     *        },
     * 		"dynamic": "",
     * 		"cid": 299899316,
     * 		"dimension": {
     * 			"width": 1920,
     * 			"height": 1080,
     * 			"rotate": 0
     *        },
     * 		"no_cache": false,
     */

    /**
     * https://api.bilibili.com/x/player/playurl?otype=json&fnver=0&fnval=2&player=1&bvid=Bvid&cid=Cid&qn=QN
     * {
     * 	"code": 0,
     * 	"message": "0",
     * 	"ttl": 1,
     * 	"data": {
     * 		"from": "local",
     * 		"result": "suee",
     * 		"message": "",
     * 		"quality": 64,
     * 		"format": "flv720",
     * 		"timelength": 270380,
     * 		"accept_format": "flv,flv720,flv480,mp4",
     * 		"accept_description": ["高清 1080P", "高清 720P", "清晰 480P", "流畅 360P"],
     * 		"accept_quality": [80, 64, 32, 16],
     * 		"video_codecid": 7,
     * 		"seek_param": "start",
     * 		"seek_type": "offset",
     * 		"durl": [{
     * 			"order": 1,
     * 			"length": 270380,
     * 			"size": 35104050,
     * 			"ahead": "",
     * 			"vhead": "",
     * 			"url": "https://xy1x181x176x122xy.mcdn.bilivideo.cn:4483/upgcxcode/16/93/299899316/299899316_nb2-1-64.flv?e=ig8euxZM2rNcNbRgnWdVhwdlhWNHhwdVhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M=\u0026uipk=5\u0026nbs=1\u0026deadline=1634893270\u0026gen=playurlv2\u0026os=mcdn\u0026oi=1033251618\u0026trid=0001eb16a1f2873548e69c158db791582b18u\u0026platform=pc\u0026upsig=f8d3f1656e93e5a0358fe3d396b74e8a\u0026uparams=e,uipk,nbs,deadline,gen,os,oi,trid,platform\u0026mcdnid=2001129\u0026mid=87038955\u0026bvc=vod\u0026nettype=0\u0026orderid=0,3\u0026agrr=1\u0026logo=A0000002",
     * 			"backup_url": ["https://xy106x42x1x111xy.mcdn.bilivideo.cn:4483/upgcxcode/16/93/299899316/299899316_nb2-1-64.flv?e=ig8euxZM2rNcNbRgnWdVhwdlhWNHhwdVhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M=\u0026uipk=5\u0026nbs=1\u0026deadline=1634893270\u0026gen=playurlv2\u0026os=mcdn\u0026oi=1033251618\u0026trid=0001eb16a1f2873548e69c158db791582b18u\u0026platform=pc\u0026upsig=f8d3f1656e93e5a0358fe3d396b74e8a\u0026uparams=e,uipk,nbs,deadline,gen,os,oi,trid,platform\u0026mcdnid=2001302\u0026mid=87038955\u0026bvc=vod\u0026nettype=0\u0026orderid=1,3\u0026agrr=1\u0026logo=60000002", "https://cn-sxxa3-dx-bcache-08.bilivideo.com/upgcxcode/16/93/299899316/299899316_nb2-1-64.flv?e=ig8euxZM2rNcNbRgnWdVhwdlhWNHhwdVhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M=\u0026uipk=5\u0026nbs=1\u0026deadline=1634893270\u0026gen=playurlv2\u0026os=bcache\u0026oi=1033251618\u0026trid=0000eb16a1f2873548e69c158db791582b18u\u0026platform=pc\u0026upsig=3d921fd3cc41b72a6d80acc9ed5e9e78\u0026uparams=e,uipk,nbs,deadline,gen,os,oi,trid,platform\u0026cdnid=63308\u0026mid=87038955\u0026bvc=vod\u0026nettype=0\u0026orderid=2,3\u0026agrr=1\u0026logo=40000000"]
     *                }],
     * 		"support_formats": [{
     * 			"quality": 80,
     * 			"format": "flv",
     * 			"new_description": "1080P 高清",
     * 			"display_desc": "1080P",
     * 			"superscript": ""
     *        }, {
     * 			"quality": 64,
     * 			"format": "flv720",
     * 			"new_description": "720P 高清",
     * 			"display_desc": "720P",
     * 			"superscript": ""
     *        }, {
     * 			"quality": 32,
     * 			"format": "flv480",
     * 			"new_description": "480P 清晰",
     * 			"display_desc": "480P",
     * 			"superscript": ""
     *        }, {
     * 			"quality": 16,
     * 			"format": "mp4",
     * 			"new_description": "360P 流畅",
     * 			"display_desc": "360P",
     * 			"superscript": ""
     *        }],
     * 		"high_format": null* 	}
     * }
     */
}