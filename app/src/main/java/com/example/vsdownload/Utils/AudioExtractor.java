package com.example.vsdownload.Utils;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.RequiresApi;

public class AudioExtractor {

    // https://www.bilibili.com/video/BV1As411a7TP 这个可以用，直接用这个
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void extractAudio(Handler mHandler, String file_in, String file_out) throws IOException{
        MediaExtractor me=new MediaExtractor();
        MediaPlayer md=new MediaPlayer();//新建一个视频播放器 用来获取视频时长
        try
        {
            md.setDataSource(file_in);
            md.prepare();
        }
        catch (SecurityException e)
        {}
        catch (IllegalArgumentException e)
        {}
        catch (IllegalStateException e)
        {}
        catch (IOException e)
        {}
        int audioIndex=-1;//音频通道
        int count=0;//已转换的时长
        int size=md.getDuration();//视频总时长
//            System.out.println("me.setDataSource(file_in): " + file_in);
        me.setDataSource(file_in);//设置要提取音频的视频路径，可以是网络链接哦
        int trackCount=me.getTrackCount();//获取通道总数
        for(int i=0;i<trackCount;i++){
            MediaFormat mf=me.getTrackFormat(i);
            if(mf.getString(MediaFormat.KEY_MIME).startsWith("audio")){//判断是否为音频通道
                audioIndex=i;
            }
        }
        me.selectTrack(audioIndex);//切换到音频通道
        MediaFormat mf=me.getTrackFormat(audioIndex);

        MediaMuxer mm=new MediaMuxer(file_out,MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);//提取mp4格式视频的音频
        int writeAuidoIndex=mm.addTrack(mf);
        mm.start();//开始提取
        ByteBuffer buff=ByteBuffer.allocate(mf.getInteger(MediaFormat.KEY_MAX_INPUT_SIZE));
        MediaCodec.BufferInfo bi=new MediaCodec.BufferInfo();
        long dtime=0;//相邻帧间隔的时间
        {
            me.readSampleData(buff,0);
            if(me.getSampleFlags()==MediaExtractor.SAMPLE_FLAG_SYNC){//判断是否为关键帧
                me.advance();
            }
            me.readSampleData(buff,0);
            long sec=me.getSampleTime();
            me.advance();
            me.readSampleData(buff,0);
            dtime=Math.abs(me.getSampleTime()-sec);
        }
        me.unselectTrack(audioIndex);
        me.selectTrack(audioIndex);//重置音频通道
        //开始提取音频
        while(true){
            int readsize=me.readSampleData(buff,0);
            if(readsize<0){
                break;
            }
            me.advance();//移动到下一帧，前面忘说了
            bi.size=readsize;
            bi.flags=me.getSampleFlags();
            bi.offset=0;
            bi.presentationTimeUs+=dtime;
            count+=dtime;//记录当前转换的时间

            int progress = (int) ((count * 1.0f / size) * 100);
            Message message = mHandler.obtainMessage(1);
            message.arg1 = progress;
            mHandler.sendMessage(message);

            mm.writeSampleData(writeAuidoIndex,buff,bi);//将提取好的音频写入文件
        }
        //停止并释放缓存
        mm.stop();
        mm.release();
        me.release();
    }
//    public static void extractMedia(String originPath, String targetDir, String songName) throws IOException {
//        FileOutputStream audioOutputStream = null;
//        MediaExtractor mediaExtractor = new MediaExtractor();
//        try {
//            //分离的音频文件
//            File audioFile = new File(targetDir, songName);
//            audioOutputStream = new FileOutputStream(audioFile);
//            mediaExtractor.setDataSource(originPath);
//            int trackCount = mediaExtractor.getTrackCount();//获得通道数量
//            int audioTrackIndex = 0;//音频轨道索引
//            MediaFormat audioMediaFormat = null;
////            //源文件 下面的报错
////            mediaExtractor.setDataSource(originPath);
//            //信道总数
//            int trackCount = mediaExtractor.getTrackCount();
//            int audioTrackIndex = -1;
//            for (int i = 0; i < trackCount; i++) {
//                MediaFormat trackFormat = mediaExtractor.getTrackFormat(i);
//                String mineType = trackFormat.getString(MediaFormat.KEY_MIME);
//                System.out.println("minetype: " + mineType);
//                //音频信道
//                if (mineType.startsWith("audio/")) {
//                    audioTrackIndex = i;
//                    audioMediaFormat = mediaExtractor.getTrackFormat(i);
//                }
//            }
//            for (int i = 0; i < trackCount; i++) { //遍历所以轨道
//                MediaFormat itemMediaFormat = mediaExtractor.getTrackFormat(i);
//                String itemMime = itemMediaFormat.getString(MediaFormat.KEY_MIME);
//                if (itemMime.startsWith("audio")) { //获取音频轨道位置
//                    audioTrackIndex = i;
//                    audioMediaFormat = itemMediaFormat;
//                    continue;
//                }
//            }
//
//            int maxAudioBufferCount = audioMediaFormat.getInteger(MediaFormat.KEY_MAX_INPUT_SIZE);//获取音频的输出缓存的最大大小
//            ByteBuffer audioByteBuffer = ByteBuffer.allocate(maxAudioBufferCount);
//            mediaExtractor.selectTrack(audioTrackIndex);//选择音频轨道
//            int len = 0;
//            while ((len = mediaExtractor.readSampleData(audioByteBuffer, 0)) != -1) {
//                System.out.println("len: " + len);
//                byte[] bytes = new byte[len];
//                audioByteBuffer.get(bytes);
//
//
//                /**
//                 * 添加adts头
//                 */
//                byte[] adtsData = new byte[len + 7];
//                addADTStoPacket(adtsData, len+7);
//                System.arraycopy(bytes, 0, adtsData, 7, len);
//
//                audioOutputStream.write(bytes);
//                audioByteBuffer.clear();
//                mediaExtractor.advance();
//            }
//
//            audioOutputStream.flush();
//            audioOutputStream.close();
//            ByteBuffer byteBuffer = ByteBuffer.allocate(500 * 1024);
//            //切换到音频信道
//            mediaExtractor.selectTrack(audioTrackIndex);
//            while (true) {
//                int readSampleCount = mediaExtractor.readSampleData(byteBuffer, 0);
//                if (readSampleCount < 0) {
//                    break;
//                }
//                //保存音频信息
//                byte[] buffer = new byte[readSampleCount];
//                byteBuffer.get(buffer);
//                audioOutputStream.write(buffer);
//                byteBuffer.clear();
//                mediaExtractor.advance();
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            mediaExtractor.release();
//            System.out.println("extract finish");
//        }
//    }
//
//    private static void addADTStoPacket(byte[] packet, int packetLen) {
//        /*
//        标识使用AAC级别 当前选择的是LC
//        一共有1: AAC Main 2:AAC LC (Low Complexity) 3:AAC SSR (Scalable Sample Rate) 4:AAC LTP (Long Term Prediction)
//        */
//        int profile = 2;
//        int frequencyIndex = 0x04; //设置采样率
//        int channelConfiguration = 2; //设置频道,其实就是声道
//
//        // fill in ADTS data
//        packet[0] = (byte) 0xFF;
//        packet[1] = (byte) 0xF9;
//        packet[2] = (byte) (((profile - 1) << 6) + (frequencyIndex << 2) + (channelConfiguration >> 2));
//        packet[3] = (byte) (((channelConfiguration & 3) << 6) + (packetLen >> 11));
//        packet[4] = (byte) ((packetLen & 0x7FF) >> 3);
//        packet[5] = (byte) (((packetLen & 7) << 5) + 0x1F);
//        packet[6] = (byte) 0xFC;
//    }
}
