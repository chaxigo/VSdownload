package com.example.vsdownload.Down;

import android.os.Handler;
import android.os.Message;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.*;
import com.example.vsdownload.MainActivity;

public class BiliDown {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private OkHttpClient client;
    private Request request;
    private Response response;
    public String title;
    public String originPath;

    private int qn = 16;

    private String getCid(String bvid, int pnum) throws IOException, JSONException {
        Request request = new Request.Builder()
                .url("https://api.bilibili.com/x/web-interface/view?bvid="+bvid)
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            JSONObject jsonObject = new JSONObject(responseBody);
            if (jsonObject.getInt("code") != 0){
                return "";
            }
            // 多p和单p
            JSONArray pages = jsonObject.getJSONObject("data").getJSONArray("pages");
            if (pages != null && pages.length()>1) {
                // multi page
                title = pages.getJSONObject(pnum-1).getString("part");
                return pages.getJSONObject(pnum-1).getString("cid");
            } else {
                title = jsonObject.getJSONObject("data").get("title").toString();
                return jsonObject.getJSONObject("data").get("cid").toString();
            }
        }
        return "";
    }

    //qn ： 视频质量         112 -> 高清 1080P+,   80 -> 高清 1080P,   64 -> 高清 720P,  32  -> 清晰 480P,  16 -> 流畅 360P
    private String getVideoUrl(String bvid, String cid, int qn) throws IOException, JSONException {
        String newURL = "";
        request = new Request.Builder()
                .url("https://api.bilibili.com/x/player/playurl?otype=json&fnver=0&fnval=2&player=1" +
                        "&bvid=" + bvid +
                        "&cid=" + cid +
                        "&qn=" + qn)
                .build();
        response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            JSONObject jsonObject = new JSONObject(responseBody);
            newURL = jsonObject.getJSONObject("data").getJSONArray("durl").getJSONObject(0).get("url").toString();
        }
        return newURL;
    };

    public String download(Handler mHandler, String bvid, int pnum, int qn,
                           String targetPath, String newTitle) throws IOException, JSONException {
        /**
         *  bvid = "1Gr4y1A7Ln";
         *  cid = "299899316";
         */
        System.out.println("start download");
        client = new OkHttpClient();
        String cid = getCid(bvid, pnum);
        if(cid=="") return "";
        String videoUrl = getVideoUrl(bvid, cid, changeQn(qn));
        request = new Request.Builder()
                .addHeader("Referer", "https://www.bilibili.com")
                .url(videoUrl)
                .build();
        response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            // 带进度条
            InputStream is = response.body().byteStream();
            String storePath = targetPath + "/" + title + ".mp4";
            if(!newTitle.equals("")){
                title = newTitle;
            }
            Message message1 = new Message();
            message1.obj = "正在下载：" + title + ".mp4";
            mHandler.sendMessage(message1);
            File file = new File(storePath);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] bytes = new byte[1024];
            int len = 0;
            long fileSize = response.body().contentLength();
            long sum = 0;
            int porSize = 0;
            while ((len = is.read(bytes)) != -1) {
                fos.write(bytes,0,len);
                sum += len;
                porSize = (int) ((sum * 1.0f / fileSize) * 100);
                Message message = mHandler.obtainMessage(1);
                message.arg1 = porSize;
                mHandler.sendMessage(message);
            }
            try{
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
//            InputStream inputStream = response.body().byteStream();
//            FileUtils.copyInputStreamToFile(inputStream,
//                    new File(targetPath + "/" + title + ".mp4"));
            System.out.println("finish download");
            return storePath;
        } else {
            System.out.println("failure");
            return "";
        }
    }

    private int changeQn(int originQn) {
        /**
         * 360 16
         * 480 32
         * 720 64
         * 1080 80
         * 108060 112
         */
        switch (originQn){
            case 360:
                return 16;
            case 480:
                return 32;
            case 720:
                return 64;
            case 1080:
                return 80;
            case 108060:
                return 112;
            default:
                return 16;
        }
    }

//    这个类使用了commons-io   网上下的参考
//    public void downloadNew(String urlPath, String descFileName) throws MalformedURLException {
//        InputStream inputStream = null;
//        try {
//            long begin = System.currentTimeMillis();
//            URL url = new URL(urlPath);
//            URLConnection urlConnection = url.openConnection();
//            urlConnection.setRequestProperty("Referer", "https://www.bilibili.com"); // 设置协议
//            urlConnection.setRequestProperty("Sec-Fetch-Mode", "no-cors");
//            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36");
//            urlConnection.setConnectTimeout(10 * 1000);
//            System.out.println("共:" + (urlConnection.getContentLength() / 1024) + "Kb");
//            System.out.println("开始下载...");
//            InputStream input = urlConnection.getInputStream();
//            FileUtils.copyInputStreamToFile(input, new File("E:\\ClassAB\\" + descFileName));
//            long end = System.currentTimeMillis();
//            System.out.println("耗时：" + (end - begin) / 1000 + "秒");
//            System.out.println("下载完成！");
//        } catch (Exception e) {
//            System.out.println("异常中止: " + e);
//        }
//    }

    /**
     * https://api.bilibili.com/x/web-interface/view?bvid=
     * {
     * 	"code": 0,
     * 	"message": "0",
     * 	"ttl": 1,
     * 	"data": {
     * 		"bvid": "BV1Gr4y1A7Ln",
     * 		"aid": 756822361,
     * 		"videos": 1,
     * 		"tid": 31,
     * 		"tname": "翻唱",
     * 		"copyright": 2,
     * 		"pic": "http://i1.hdslb.com/bfs/archive/a74a2af22ed5045ab44ba5bdb0baf2bb4ee450ad.jpg",
     * 		"title": "【阿梓】《最长的电影》都快听哭了，神回歌曲",
     * 		"pubdate": 1613747308,
     * 		"ctime": 1613747308,
     * 		"desc": "bilibili频道：阿梓从小就很可爱\nhttps://space.bilibili.com/7706705/\n微博：阿梓从小就很可爱\n网易云：阿梓从小就很可爱\n切片来自2020.12.22\n感觉挺好听的。",
     * 		"desc_v2": [{
     * 			"raw_text": "bilibili频道：阿梓从小就很可爱\nhttps://space.bilibili.com/7706705/\n微博：阿梓从小就很可爱\n网易云：阿梓从小就很可爱\n切片来自2020.12.22\n感觉挺好听的。",
     * 			"type": 1,
     * 			"biz_id": 0
     *                }],
     * 		"state": 0,
     * 		"duration": 271,
     * 		"rights": {
     * 			"bp": 0,
     * 			"elec": 0,
     * 			"download": 1,
     * 			"movie": 0,
     * 			"pay": 0,
     * 			"hd5": 0,
     * 			"no_reprint": 0,
     * 			"autoplay": 1,
     * 			"ugc_pay": 0,
     * 			"is_cooperation": 0,
     * 			"ugc_pay_preview": 0,
     * 			"no_background": 0,
     * 			"clean_mode": 0,
     * 			"is_stein_gate": 0,
     * 			"is_360": 0
     *        },
     * 		"owner": {
     * 			"mid": 1091772183,
     * 			"name": "1927的索尔维",
     * 			"face": "http://i0.hdslb.com/bfs/face/9836d9ed85b2e79a7ef5db32d6817ef23fa6310a.jpg"
     *        },
     * 		"stat": {
     * 			"aid": 756822361,
     * 			"view": 83249,
     * 			"danmaku": 53,
     * 			"reply": 124,
     * 			"favorite": 2257,
     * 			"coin": 504,
     * 			"share": 482,
     * 			"now_rank": 0,
     * 			"his_rank": 0,
     * 			"like": 1956,
     * 			"dislike": 0,
     * 			"evaluation": "",
     * 			"argue_msg": ""
     *        },
     * 		"dynamic": "",
     * 		"cid": 299899316,
     * 		"dimension": {
     * 			"width": 1920,
     * 			"height": 1080,
     * 			"rotate": 0
     *        },
     * 		"no_cache": false,
     */

    /** // 多p
     * {
     * 	"code": 0,
     * 	"message": "0",
     * 	"ttl": 1,
     * 	"data": {
     * 		"bvid": "BV1F64y1e7a1",
     * 		"aid": 762366961,
     * 		"videos": 2,
     * 		"tid": 31,
     * 		"tname": "翻唱",
     * 		"copyright": 2,
     * 		"pic": "http://i1.hdslb.com/bfs/archive/7823c1c867e1259ead6c307a3cc17f807882a9fd.jpg",
     * 		"title": "【阿梓歌】《泡沫》！！！纯享版！",
     * 		"pubdate": 1628866387,
     * 		"ctime": 1628866387,
     * 		"desc": "510阿梓录播\n剪辑:CC无名酱\n私藏自用 梓宝永远滴神！！！\n▶bilibili频道：阿梓从小就很可爱\nhttps://space.bilibili.com/7706705/\n▶微博：阿梓从小就很可爱\n▶网易云：阿梓从小就很可爱",
     * 		"desc_v2": [{
     * 			"raw_text": "510阿梓录播\n剪辑:CC无名酱\n私藏自用 梓宝永远滴神！！！\n▶bilibili频道：阿梓从小就很可爱\nhttps://space.bilibili.com/7706705/\n▶微博：阿梓从小就很可爱\n▶网易云：阿梓从小就很可爱",
     * 			"type": 1,
     * 			"biz_id": 0
     *                }],
     * 		"state": 0,
     * 		"duration": 507,
     * 		"rights": {
     * 			"bp": 0,
     * 			"elec": 0,
     * 			"download": 1,
     * 			"movie": 0,
     * 			"pay": 0,
     * 			"hd5": 0,
     * 			"no_reprint": 0,
     * 			"autoplay": 1,
     * 			"ugc_pay": 0,
     * 			"is_cooperation": 0,
     * 			"ugc_pay_preview": 0,
     * 			"no_background": 0,
     * 			"clean_mode": 0,
     * 			"is_stein_gate": 0,
     * 			"is_360": 0
     *        },
     * 		"owner": {
     * 			"mid": 1982780,
     * 			"name": "CC无名酱",
     * 			"face": "http://i2.hdslb.com/bfs/face/5f71e7b138a17a15dcde88df688165e8bf94a274.jpg"
     *        },
     * 		"stat": {
     * 			"aid": 762366961,
     * 			"view": 148995,
     * 			"danmaku": 282,
     * 			"reply": 771,
     * 			"favorite": 4029,
     * 			"coin": 1361,
     * 			"share": 2005,
     * 			"now_rank": 0,
     * 			"his_rank": 0,
     * 			"like": 4690,
     * 			"dislike": 0,
     * 			"evaluation": "",
     * 			"argue_msg": ""
     *        },
     * 		"dynamic": "",
     * 		"cid": 388524618,
     * 		"dimension": {
     * 			"width": 1920,
     * 			"height": 1080,
     * 			"rotate": 0
     *        },
     * 		"no_cache": false,
     * 		"pages": [{
     * 			"cid": 388524618,
     * 			"page": 1,
     * 			"from": "vupload",
     * 			"part": "阿梓 泡沫纯享",
     * 			"duration": 253,
     * 			"vid": "",
     * 			"weblink": "",
     * 			"dimension": {
     * 				"width": 1920,
     * 				"height": 1080,
     * 				"rotate": 0
     *            },
     * 			"first_frame": "http://i2.hdslb.com/bfs/storyff/n210813a23gjsh53k1hqis3td2n4epp9_firsti.jpg"
     *        }, {
     * 			"cid": 400771646,
     * 			"page": 2,
     * 			"from": "vupload",
     * 			"part": "阿梓 泡沫纯享9.1",
     * 			"duration": 254,
     * 			"vid": "",
     * 			"weblink": "",
     * 			"dimension": {
     * 				"width": 1920,
     * 				"height": 1080,
     * 				"rotate": 0
     *            },
     * 			"first_frame": "http://i0.hdslb.com/bfs/storyff/n210901a22qztbj334ssv1twshq6tn9f_firsti.jpg"
     *        }],
     * 		"subtitle": {
     * 			"allow_submit": true,
     * 			"list": []
     *        },
     * 		"user_garb": {
     * 			"url_image_ani_cut": ""
     *        }* 	}
     * }
     */

    /**
     * https://api.bilibili.com/x/player/playurl?otype=json&fnver=0&fnval=2&player=1&bvid=Bvid&cid=Cid&qn=QN
     * {
     * 	"code": 0,
     * 	"message": "0",
     * 	"ttl": 1,
     * 	"data": {
     * 		"from": "local",
     * 		"result": "suee",
     * 		"message": "",
     * 		"quality": 64,
     * 		"format": "flv720",
     * 		"timelength": 270380,
     * 		"accept_format": "flv,flv720,flv480,mp4",
     * 		"accept_description": ["高清 1080P", "高清 720P", "清晰 480P", "流畅 360P"],
     * 		"accept_quality": [80, 64, 32, 16],
     * 		"video_codecid": 7,
     * 		"seek_param": "start",
     * 		"seek_type": "offset",
     * 		"durl": [{
     * 			"order": 1,
     * 			"length": 270380,
     * 			"size": 35104050,
     * 			"ahead": "",
     * 			"vhead": "",
     * 			"url": "https://xy1x181x176x122xy.mcdn.bilivideo.cn:4483/upgcxcode/16/93/299899316/299899316_nb2-1-64.flv?e=ig8euxZM2rNcNbRgnWdVhwdlhWNHhwdVhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M=\u0026uipk=5\u0026nbs=1\u0026deadline=1634893270\u0026gen=playurlv2\u0026os=mcdn\u0026oi=1033251618\u0026trid=0001eb16a1f2873548e69c158db791582b18u\u0026platform=pc\u0026upsig=f8d3f1656e93e5a0358fe3d396b74e8a\u0026uparams=e,uipk,nbs,deadline,gen,os,oi,trid,platform\u0026mcdnid=2001129\u0026mid=87038955\u0026bvc=vod\u0026nettype=0\u0026orderid=0,3\u0026agrr=1\u0026logo=A0000002",
     * 			"backup_url": ["https://xy106x42x1x111xy.mcdn.bilivideo.cn:4483/upgcxcode/16/93/299899316/299899316_nb2-1-64.flv?e=ig8euxZM2rNcNbRgnWdVhwdlhWNHhwdVhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M=\u0026uipk=5\u0026nbs=1\u0026deadline=1634893270\u0026gen=playurlv2\u0026os=mcdn\u0026oi=1033251618\u0026trid=0001eb16a1f2873548e69c158db791582b18u\u0026platform=pc\u0026upsig=f8d3f1656e93e5a0358fe3d396b74e8a\u0026uparams=e,uipk,nbs,deadline,gen,os,oi,trid,platform\u0026mcdnid=2001302\u0026mid=87038955\u0026bvc=vod\u0026nettype=0\u0026orderid=1,3\u0026agrr=1\u0026logo=60000002", "https://cn-sxxa3-dx-bcache-08.bilivideo.com/upgcxcode/16/93/299899316/299899316_nb2-1-64.flv?e=ig8euxZM2rNcNbRgnWdVhwdlhWNHhwdVhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M=\u0026uipk=5\u0026nbs=1\u0026deadline=1634893270\u0026gen=playurlv2\u0026os=bcache\u0026oi=1033251618\u0026trid=0000eb16a1f2873548e69c158db791582b18u\u0026platform=pc\u0026upsig=3d921fd3cc41b72a6d80acc9ed5e9e78\u0026uparams=e,uipk,nbs,deadline,gen,os,oi,trid,platform\u0026cdnid=63308\u0026mid=87038955\u0026bvc=vod\u0026nettype=0\u0026orderid=2,3\u0026agrr=1\u0026logo=40000000"]
     *                }],
     * 		"support_formats": [{
     * 			"quality": 80,
     * 			"format": "flv",
     * 			"new_description": "1080P 高清",
     * 			"display_desc": "1080P",
     * 			"superscript": ""
     *        }, {
     * 			"quality": 64,
     * 			"format": "flv720",
     * 			"new_description": "720P 高清",
     * 			"display_desc": "720P",
     * 			"superscript": ""
     *        }, {
     * 			"quality": 32,
     * 			"format": "flv480",
     * 			"new_description": "480P 清晰",
     * 			"display_desc": "480P",
     * 			"superscript": ""
     *        }, {
     * 			"quality": 16,
     * 			"format": "mp4",
     * 			"new_description": "360P 流畅",
     * 			"display_desc": "360P",
     * 			"superscript": ""
     *        }],
     * 		"high_format": null* 	}
     * }
     */
}