package com.example.vsdownload;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.example.vsdownload.Down.BiliDown;
import com.example.vsdownload.Utils.AudioExtractor;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.BreakIterator;
import java.util.Arrays;
import java.util.LinkedList;

import com.example.vsdownload.Permission.StoragePermission;

import org.json.JSONException;

@SuppressLint("HandlerLeak")
public class MainActivity extends AppCompatActivity {

    public static String videoPath = Environment.getExternalStorageDirectory().toString()
            + "/DCIM/Camera";
    public static String musicPath = Environment.getExternalStorageDirectory().toString()
            + "/Music";
    BiliDown biliDown = new BiliDown();
    AudioExtractor audioExtractor = new AudioExtractor();

    EditText bvidText, pnumText, qnText, renameText;
    TextView situationText;
    Button downloadBtn, selectFileBtn, extractBtn;
    ProgressBar progressBar;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
//            System.out.println("msg: " + msg.obj);
//            System.out.println("msg.arg1: "+msg.arg1+" msg.obj: "+msg.obj);
            if(msg.obj != null){
                //知识传递文字
//                System.out.println("else");
                situationText.setText(msg.obj.toString());
            } else {
//                System.out.println("if");
                progressBar.setProgress(msg.arg1);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StoragePermission.verifyPermissions(this);
//        System.out.println("thisone "+Environment.getExternalStorageDirectory().toString());
        setContentView(R.layout.activity_main);

        bvidText = findViewById(R.id.bvid);
        pnumText = findViewById(R.id.pnum);
        qnText = findViewById(R.id.qn);
        renameText = findViewById(R.id.rename);
        situationText = findViewById(R.id.situationText);
        progressBar = findViewById(R.id.progressBar);

        downloadBtn = findViewById(R.id.download);
        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Android 4.0 之后不能在主线程中请求HTTP请求
                new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            String bvid, newTitle;
                            int pnum=1, qn=16;
                            newTitle = renameText.getText().toString();
                            bvid = bvidText.getText().toString();
                            if(bvid.startsWith("bv") || bvid.startsWith("BV")
                                    || bvid.startsWith("Bv") || bvid.startsWith("bV")){
                                bvid = bvid.substring(2);
                            }
                            if(!pnumText.getText().toString().equals("")) {
                                try {
                                    pnum = Integer.parseInt(pnumText.getText().toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Message message4 = new Message();
                                    message4.obj = "p数填写数字";
                                    mHandler.sendMessage(message4);
                                    return;
                                }
                            }
                            if(!qnText.getText().toString().equals("")){
                                try {
                                    qn = Integer.parseInt(qnText.getText().toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Message message4 = new Message();
                                    message4.obj = "质量请填写数字";
                                    mHandler.sendMessage(message4);
                                    return;
                                }
                            }
                            String storePath = biliDown.download(mHandler,bvid,pnum,qn,videoPath,newTitle);
                            if(storePath.equals("")){
                                Message message2 = new Message();
                                message2.obj = "请求错误";
                                mHandler.sendMessage(message2);
                                return;
                            } else {
                                Message message3 = new Message();
                                message3.obj = "下载完成: "+storePath;
                                mHandler.sendMessage(message3);
                                //更新数据库
                                updateFile(storePath);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

        selectFileBtn = findViewById(R.id.selectFile);
        selectFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*"); //选择视频 （mp4 3gp 是android支持的视频格式）
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, 1);
            }
        });

        extractBtn = findViewById(R.id.extract);
        extractBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
            @Override
            public void onClick(View view) {
                String storePath = situationText.getText().toString();
                String title = getTitle(storePath);
                String newTitle = renameText.getText().toString();
                if(!newTitle.equals("")){
                    title = newTitle;
                }
                if(title == ""){
                    Message message1 = new Message();
                    message1.obj = "未选择文件或文件名不合规";
                    mHandler.sendMessage(message1);
                    return;
                }
                File file = new File(storePath);
                if(!file.exists()){
                    Message message2 = new Message();
                    message2.obj = "上方显示的是文件名，不合规，请重选";
                    mHandler.sendMessage(message2);
                }
                System.out.println("getTitle " + title);
                String finalTitle = title;
                new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            Message message1 = new Message();
                            message1.obj = "开始提取:"+storePath;
                            mHandler.sendMessage(message1);
                            audioExtractor.extractAudio(mHandler, storePath, musicPath+"/"+ finalTitle +".mp3");
                            Message message2 = new Message();
                            message2.obj = "提取完成,存储到:"+musicPath+"/"+ finalTitle +".mp3";
                            mHandler.sendMessage(message2);
                            // 更新音乐
                            updateFile(musicPath+"/"+ finalTitle +".mp3");
                        } catch (IOException e) {
                            e.printStackTrace();
                            Message message1 = new Message();
                            message1.obj = "视频格式不支持，仅支持mp4和3gp";
                            mHandler.sendMessage(message1);
                            return;
                        }
                    }
                }).start();
            }
        });
    }

    private void updateFile(String filename)//filename是我们的文件全名，包括后缀哦
    {
        MediaScannerConnection.scanFile(this,
                new String[] { filename }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        System.out.println("ExternalStorage"+ "Scanned " + path + ":");
                        System.out.println("ExternalStorage"+ "-> uri=" + uri);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            String path;
            if ("file".equalsIgnoreCase(uri.getScheme())) {//使用第三方应用打开
                path = uri.getPath();
                situationText.setText(path);
                Toast.makeText(this, path + "11111", Toast.LENGTH_SHORT).show();
                return;
            }
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {//4.4以后
                path = getPath(this, uri);
                situationText.setText(path);
                Toast.makeText(this, path, Toast.LENGTH_SHORT).show();
            } else {//4.4以下下系统调用方法
                path = getRealPathFromURI(uri);
                situationText.setText(path);
                Toast.makeText(MainActivity.this, path + "222222", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (null != cursor && cursor.moveToFirst()) {
            ;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            cursor.close();
        }
        return res;
    }

    /**
     * 专为Android4.4设计的从Uri获取文件绝对路径，以前的方法已不好使
     */
    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private String getTitle(String path) {
        int start = path.lastIndexOf("/");
        int end = path.lastIndexOf(".");
        if (start>=end) {
            return "";
        }
        return path.substring(start+1, end);
    }


}